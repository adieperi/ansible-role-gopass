# ansible-role-gopass
Ansible role for deploying the gopass binary

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://gitlab.com/adieperi/ansible-role-gopass.git
  scm: git
  version: main
  name: ansible-role-gopass
```
### Download the role
```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.9 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `gopass_version` | 'latest' | [version](https://github.com/gopasspw/gopass/tags) | Choice of the gopass version. |

## Example Playbook

```yaml
---
- hosts: all
  tasks:
    - name: Install gopass
      include_role:
        name: ansible-role-gopass
      vars:
        gopass_version: 1.14.8
```
## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
